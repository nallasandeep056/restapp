package org.restful.webapp.restApp.resources.beans;

import javax.ws.rs.QueryParam;

public class BeanParams {

	private @QueryParam("year") int year;
	private @QueryParam("offset") int offSet;
	private @QueryParam("size") int size;
	
	public int getYear() {
		return year;
	}
	public void setYear(int year) {
		this.year = year;
	}
	public int getOffSet() {
		return offSet;
	}
	public void setOffSet(int offSet) {
		this.offSet = offSet;
	}
	public int getSize() {
		return size;
	}
	public void setSize(int size) {
		this.size = size;
	}
}
