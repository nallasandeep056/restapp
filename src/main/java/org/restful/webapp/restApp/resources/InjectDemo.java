package org.restful.webapp.restApp.resources;

import javax.ws.rs.Consumes;
import javax.ws.rs.CookieParam;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.MatrixParam;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriInfo;

@Path("injectdemo")
@Consumes(MediaType.TEXT_PLAIN)
@Produces(MediaType.TEXT_PLAIN)
public class InjectDemo {
	@GET
	@Path("annotations")
	public String annotations(@MatrixParam("param") String matrixParam,
							@HeaderParam("headerParam") String headerParam,
							@CookieParam("name") String cookieParam) {
		return "Matrix Param: "+matrixParam+"; Header Param: "+headerParam+"; Cookie Param: "+cookieParam;
	}

	@GET
	@Path("context")
	public String context(@Context UriInfo uriInfo, @Context HttpHeaders header) {
		String path = uriInfo.getAbsolutePath().toString();
		String cookies = header.getCookies().toString();
		return "Path: " + path + "; Cookies: " + cookies;
	}
}
