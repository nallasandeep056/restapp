package org.restful.webapp.restApp.resources;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.restful.webapp.restApp.model.Profile;
import org.restful.webapp.restApp.service.ProfileService;

@Path("/profiles")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class ProfileResources {

	private ProfileService profileServie = new ProfileService();
	@GET
	public List<Profile> getProfiles(@QueryParam("year") int year,
			@QueryParam("offset") int offSet, @QueryParam("size") int size) {
		if(year > 0) {
			return profileServie.getProfilesByYear(year);
		}
		if(offSet >= 0 && size >= 0) {
			return profileServie.getProfilesByPage(offSet, size);
		}
 		return profileServie.getProfiles();
	}
	
	@POST
	public Profile addProfile(Profile profile) {
		return profileServie.addProfile(profile);
	}
	
	@GET
	@Path("/{profileId}")
	public Profile getProfile(@PathParam("profileId") String profileId) {
		return profileServie.getProfile(profileId);
	}
	
	@PUT
	@Path("/{profileId}")
	public Profile addProfile(@PathParam("profileId") String profileId,Profile profile) {
		profile.setProfileName(profileId);
		return profileServie.updateProfile(profile);
	}
	
	@DELETE
	@Path("/{profileId}")
	public Profile deleteProfile(@PathParam("profileId") String profileId) {
		return profileServie.removeProfile(profileId);
	}
}
