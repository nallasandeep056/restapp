package org.restful.webapp.restApp.resources;

import java.util.List;

import javax.ws.rs.BeanParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.restful.webapp.restApp.model.Message;
import org.restful.webapp.restApp.resources.beans.BeanParams;
import org.restful.webapp.restApp.service.MessageService;

@Path("/messages")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class MessageResources {

	private MessageService messageService = new MessageService();
	@GET
	@Produces(MediaType.TEXT_XML)
	public List<Message> getMessages(@BeanParam BeanParams beanParams) {
		if(beanParams.getYear() > 0) {
			return messageService.getMessagesByYear(beanParams.getYear());
		} else if(beanParams.getOffSet() == 0 && beanParams.getSize() == 0) {
			return messageService.getMessages();
		} else if(beanParams.getOffSet() >= 0 && beanParams.getSize() >= 0) {
			return messageService.getMessagesByPage(beanParams.getOffSet(),
					beanParams.getSize());
		} else {
			return messageService.getMessages();
		}
	}
	
	@POST
	@Consumes(MediaType.TEXT_XML)
	public Response addMessage(Message message, @Context UriInfo uriInfo ) {
		Message newMessage = messageService.addMessage(message);
		return Response.created(uriInfo.getAbsolutePathBuilder().path(newMessage.getId().toString()).build())
				.entity(newMessage)
				.build();
		/*return messageService.addMessage(message);*/
	}
	
	@GET
	@Path("/{messageId}")
	public Message getMessage(@PathParam("messageId") Long messageId, @Context UriInfo uriInfo) {
		Message message = messageService.getMessage(messageId);
		message.addLinks(getUriForSelf(uriInfo, message), "self");
		message.addLinks(getUriForProfiles(uriInfo, message), "profiles");
		message.addLinks(getUriForComments(uriInfo, message), "comments");
		return message;
	}

	private String getUriForComments(UriInfo uriInfo, Message message) {
		return uriInfo.getBaseUriBuilder()
				.path(MessageResources.class)
				.path(MessageResources.class, "getComments")
				.path(CommentResources.class)
				.resolveTemplate("messageId", message.getId())
				.build()
				.toString();
	}

	private String getUriForProfiles(UriInfo uriInfo, Message message) {
		return uriInfo.getBaseUriBuilder()
				.path(ProfileResources.class)
				.build()
				.toString();
	}

	private String getUriForSelf(UriInfo uriInfo, Message message) {
		return uriInfo.getBaseUriBuilder()
				.path(MessageResources.class)
				.path(Long.toString(message.getId()))
				.build()
				.toString();
	}
	
	@PUT
	@Path("/{messageId}")
	public Message addMessage(@PathParam("messageId") Long messageId,Message message) {
		message.setId(messageId);
		return messageService.updateMessage(messageId, message);
	}
	
	@DELETE
	@Path("/{messageId}")
	public Message deleteMessage(@PathParam("messageId") Long messageId) {
		return messageService.removeMessage(messageId);
	}
	
	@Path("/{messageId}/comments")
	public CommentResources getComments() {
		return new CommentResources();
	}
}
