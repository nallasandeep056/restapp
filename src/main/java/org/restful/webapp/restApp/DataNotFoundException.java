package org.restful.webapp.restApp;



public class DataNotFoundException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5335611466989465993L;

	public DataNotFoundException(String message) {
		super(message);
	}
}
