package org.restful.webapp.restApp.database;

import java.util.HashMap;
import java.util.Map;

import org.restful.webapp.restApp.model.Comment;
import org.restful.webapp.restApp.model.Message;
import org.restful.webapp.restApp.model.Profile;

public class Database {
	
	private static Map<Long,Message> messages = new HashMap<> ();
	private static Map<String,Profile> profiles = new HashMap<> ();
	private static Map<Long,Comment> comments = new HashMap<> ();
	
	public static Map<Long, Comment> getComments() {
		return comments;
	}

	public static Map<Long,Message> getMessages() {
		return messages;
	}
	
	public static Map<String,Profile> getProfiles() {
		return profiles;
	}

}
