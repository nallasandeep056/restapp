package org.restful.webapp.restApp;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;


import org.restful.webapp.restApp.model.ErrorMessage;


public class ThrowableExceptionMapper implements ExceptionMapper<Throwable> {

	@Override
	public Response toResponse(Throwable exception) {
		ErrorMessage error = new ErrorMessage(500,exception.getMessage(),"Documentation");
		return Response.status(Status.INTERNAL_SERVER_ERROR)
						.entity(error)
						.build();
	}
}
