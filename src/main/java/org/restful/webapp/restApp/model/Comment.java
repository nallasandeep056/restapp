package org.restful.webapp.restApp.model;

import java.util.Date;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Comment {
	
	private Long id;
	private String message;
	private Date date;
	private String author;
	
	public Comment() {
		// TODO Auto-generated constructor stub
	}

	public Comment(Long id, String message, String author) {
		this.date = new Date();
		this.id = id;
		this.message = message;
		this.author = author;
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	
}
