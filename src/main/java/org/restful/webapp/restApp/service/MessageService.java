package org.restful.webapp.restApp.service;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.restful.webapp.restApp.DataNotFoundException;
import org.restful.webapp.restApp.database.Database;
import org.restful.webapp.restApp.model.Message;

public class MessageService {
	
	private Map<Long,Message> map = Database.getMessages();
	
	public MessageService(){
		map.put(1L, new Message(1L, "Hello world", "Sandeep"));
		map.put(2L, new Message(2L, "Hello buddy", "Sandeep"));
	}

	public List<Message> getMessages(){
		return new ArrayList<Message>(map.values());
	}
	
	public List<Message> getMessagesByYear(int year){
		List<Message> messages = new ArrayList<Message>();
		Calendar cal = Calendar.getInstance();
		for(Message message: map.values()) {
			cal.setTime(message.getDate());
			if(cal.get(Calendar.YEAR) == year) {
				messages.add(message);
			}
		}
		return messages;
	}
	
	public List<Message> getMessagesByPage(int offSet, int size) {
		
		List<Message> messages = new ArrayList<Message>(map.values());
		if((offSet + size) > messages.size()) {
			return new ArrayList<Message>();
		}
		return messages.subList(offSet, offSet + size);
	}
	
	public Message getMessage(Long id) {
		Message message = map.get(id);
		if(message == null) {
			throw new DataNotFoundException("Message id "+id+" is not found");
		}
		return map.get(id);
	}

	public Message addMessage(Message message) {
		message.setId(map.size()+1L);
		message.setDate(new Date());
		map.put(message.getId(), message);
		return message;
	}
	
	public Message updateMessage(Long id, Message message) {
		if(id <= 0) {
			return null;
		} else {
			map.put(id, message);
			return message;
		}
	}
	
	public Message removeMessage(Long id) {
		return map.remove(id);
	}
	
}
