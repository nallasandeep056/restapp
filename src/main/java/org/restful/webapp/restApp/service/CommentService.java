package org.restful.webapp.restApp.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.ws.rs.NotFoundException;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.restful.webapp.restApp.database.Database;
import org.restful.webapp.restApp.model.Comment;
import org.restful.webapp.restApp.model.ErrorMessage;
import org.restful.webapp.restApp.model.Message;

public class CommentService {
	
	private Map<Long,Message> messages = Database.getMessages();
	private Map<Long,Comment> comments = Database.getComments();
	
	public CommentService(){
		messages.get(1L).getComments().put(1L, new Comment(1L, "Message 1 - Comment 1 - Hello world", "Sandeep"));
		messages.get(1L).getComments().put(2L, new Comment(2L, "Message 1 - Comment 2 - Hello buddy", "Sandeep"));
		messages.get(2L).getComments().put(1L, new Comment(1L, "Message 2 - Comment 1 - Hello world", "Sandeep"));
		messages.get(2L).getComments().put(2L, new Comment(2L, "Message 2 - Comment 2 - Hello buddy", "Sandeep"));
	}

	public List<Comment> getComments(Long messageId){
		comments = messages.get(messageId).getComments();
		return new ArrayList<Comment>(comments.values());
	}
	
	/*public List<Comment> getCommentsByYear(int year){
		List<Comment> messages = new ArrayList<Comment>();
		Calendar cal = Calendar.getInstance();
		for(Comment message: map.values()) {
			cal.setTime(message.getDate());
			if(cal.get(Calendar.YEAR) == year) {
				messages.add(message);
			}
		}
		return messages;
	}
	
	public List<Comment> getCommentsByPage(int offSet, int size) {
		
		List<Comment> messages = new ArrayList<Comment>(map.values());
		if((offSet + size) > messages.size()) {
			return new ArrayList<Comment>();
		}
		return messages.subList(offSet, offSet + size);
	}
	*/
	public Comment getComment(Long messageId, Long commentId) {
		ErrorMessage error = new ErrorMessage(404,"Exception message","Documentation");
		/*Response response = Response.status(Status.NOT_FOUND)
						.entity(error)
						.build();*/
		Message message = messages.get(messageId);
		if(message == null) {
			throw new WebApplicationException(Status.NOT_FOUND);
		}
		comments = message.getComments();
		Comment comment = comments.get(commentId);
		if(comment == null) {
			throw new NotFoundException();
		}
		return comments.get(commentId);
	}

	public Comment addComment(Long messageId, Comment comment) {
		comments = messages.get(messageId).getComments();
		comment.setId(comments.size()+1L);
		comment.setDate(new Date());
		comments.put(comment.getId(), comment);
		return comment;
	}
	
	public Comment updateComment(Long messageId, Comment comment) {
		comments = messages.get(messageId).getComments();
		if(comment.getId() <= 0) {
			return null;
		} else {
			comments.put(comment.getId(), comment);
			return comment;
		}
	}
	
	public Comment removeComment(Long messageId, Long commentId) {
		comments = messages.get(messageId).getComments();
		return comments.remove(commentId);
	}
	
}
