package org.restful.webapp.restApp.service;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.restful.webapp.restApp.database.Database;
import org.restful.webapp.restApp.model.Profile;

public class ProfileService {
	
	private Map<String,Profile> map = Database.getProfiles();
	
	public ProfileService(){
		map.put("sandeep", new Profile(1L, "sandeep", "Sandeep", "Nalla"));
		map.put("vidya", new Profile(2L, "vidya", "Vidyanath", "Nalla"));
	}

	public List<Profile> getProfiles(){
		return new ArrayList<Profile>(map.values());
	}
	
	public List<Profile> getProfilesByYear(int year){
		List<Profile> profiles = new ArrayList<Profile>();
		Calendar cal = Calendar.getInstance();
		for(Profile profile: map.values()) {
			cal.setTime(profile.getCreated());
			if(cal.get(Calendar.YEAR) == year) {
				profiles.add(profile);
			}
		}
		return profiles;
	}
	
	public List<Profile> getProfilesByPage(int offSet, int size) {
		
		List<Profile> profiles = new ArrayList<Profile>(map.values());
		if((offSet+size) > profiles.size()) {
			return new ArrayList<Profile>();
		}
		return profiles.subList(offSet, offSet+size);
	}
	
	public Profile getProfile(String id) {
		return map.get(id);
	}

	public Profile addProfile(Profile profile) {
		profile.setId(map.size()+1L);
		profile.setCreated(new Date());
		map.put(profile.getProfileName(), profile);
		return profile;
	}
	
	public Profile updateProfile(Profile profile) {
		if(profile.getProfileName().isEmpty()) {
			return null;
		} else {
			map.put(profile.getProfileName(), profile);
			return profile;
		}
	}
	
	public Profile removeProfile(String profileName) {
		return map.remove(profileName);
	}
}